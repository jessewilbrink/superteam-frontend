import { Injectable } from '@angular/core';
import { IUser } from '../user';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, from, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import 'jquery';
import 'bootstrap-notify';



@Injectable({
  providedIn: 'root'
})
export class UserService {
  private getUrl = "http://localhost:8080/user/get/1";
  private putUrl = "http://localhost:8080/user/update/";

  constructor(private http: HttpClient) { }

  getUser(userID: Number): Observable<IUser> {
    // this.getUrl = this.getUrl + userID
    return this.http.get<IUser>(this.getUrl).pipe(
      tap(data => console.log('All: ' + JSON.stringify(data)))
      , catchError(this.handleError)
    );
  }

  updateUser(user: IUser): void {
    this.http.put(this.putUrl, user).subscribe(

      data => {

        $['notify']({
          // options          
          message: 'Gegevens opgeslagen'
        }, {
          // settings
          type: 'success'
        });

      },

      error => {

        $['notify']({
          // options
          message: 'Er is iets mis gegaan bij het opslaan, probeer het opnieuw'
        }, {
          // settings
          type: 'danger'
        });

      }

    );
  };

  private handleError(err: HttpErrorResponse) {
    let errorMessage = " ";
    if (err.error instanceof ErrorEvent) {
      errorMessage = 'Er is een fout opgetreden: ${ err.error.message }';
    } else {
      errorMessage = 'De server retourneerd code: ${err.status}, foutbericht is: ${err.message}';
    }
    console.error(errorMessage);
    return throwError(errorMessage);
  }
}
