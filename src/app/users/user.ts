export interface IUser {
    id: number;
    wachtwoord: String;
    emailadres: String;
    voornaam: String;
    tussenvoegsel: String;
    achternaam: String;
    adres: String;
    plaats: String;
    postcode: String;
}