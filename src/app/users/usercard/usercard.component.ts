import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-usercard',
  templateUrl: './usercard.component.html',
  styleUrls: ['./usercard.component.css']
})
export class UsercardComponent implements OnInit {

  constructor() { }

  naam: String;
  nickname: String;
  profilepic: String;
  omslagpic: String;
  bericht: String;
  ngOnInit() {
    this.naam = "Jesse Wilbrink"
    this.nickname = "jewi32"
    this.profilepic = "assets/img/faces/jesse.jpg"
    this.omslagpic = "assets/img/omslag/omslag.jpg"
    this.bericht = "testbericht";
  }

}
