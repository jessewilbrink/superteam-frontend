import { Component, OnInit } from '@angular/core';
import { IUser } from '../user';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-useredit',
  templateUrl: './useredit.component.html',
  styleUrls: ['./useredit.component.css']
})
export class UsereditComponent implements OnInit {
  user: IUser;
  errorMessage: "Gegevens zijn niet opgehaald";
  constructor(private userService: UserService) {

  }



  opslaan(): void {
    this.userService.updateUser(this.user);
  }

  ngOnInit() {
    this.userService.getUser(14).subscribe(
      user => this.user = user,
      error => this.errorMessage = <any>error
    );
  }

}
