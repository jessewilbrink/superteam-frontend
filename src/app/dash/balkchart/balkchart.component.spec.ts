import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BalkchartComponent } from './balkchart.component';

describe('BalkchartComponent', () => {
  let component: BalkchartComponent;
  let fixture: ComponentFixture<BalkchartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BalkchartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BalkchartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
