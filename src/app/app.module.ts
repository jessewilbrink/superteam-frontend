import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navigatie/navbar/navbar.component';
import { BodyComponent } from './maincomponents/body/body.component';
import { SidenavComponent } from './navigatie/sidenav/sidenav.component';
import { FooterComponent } from './maincomponents/footer/footer.component';
import { UsercardComponent } from './users/usercard/usercard.component';
import { UserpageComponent } from './users/userpage/userpage.component';
import { UsereditComponent } from './users/useredit/useredit.component';
import { PiechartComponent } from './dash/piechart/piechart.component';
import { DashboardpageComponent } from './dash/dashboardpage/dashboardpage.component';
import { LinechartComponent } from './dash/linechart/linechart.component';
import { BalkchartComponent } from './dash/balkchart/balkchart.component';
import { HttpClientModule } from '@angular/common/http';
import { PagenotfoundComponent } from './maincomponents/pagenotfound/pagenotfound.component';
import 'bootstrap-notify';
import { MijnteamComponent } from './mijnteam/mijnteam.component';
import { MijnopstellingComponent } from './mijnteam/mijnopstelling/mijnopstelling.component';
import { MijnopstellingkleinComponent } from './mijnteam/mijnopstelling/mijnopstellingklein/mijnopstellingklein.component';
import { OpstellingcreatorComponent } from './mijnteam/opstellingcreator/opstellingcreator.component';
import { SpelersoverzichtComponent } from './mijnteam/spelersoverzicht/spelersoverzicht.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    BodyComponent,
    SidenavComponent,
    FooterComponent,
    UsercardComponent,
    UserpageComponent,
    UsereditComponent,
    PiechartComponent,
    DashboardpageComponent,
    LinechartComponent,
    BalkchartComponent,
    PagenotfoundComponent,
    MijnteamComponent,
    MijnopstellingComponent,
    MijnopstellingkleinComponent,
    OpstellingcreatorComponent,
    SpelersoverzichtComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
