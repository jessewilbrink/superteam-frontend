import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserpageComponent } from './users/userpage/userpage.component';
import { DashboardpageComponent } from './dash/dashboardpage/dashboardpage.component';
import { PagenotfoundComponent } from './maincomponents/pagenotfound/pagenotfound.component';
import { MijnteamComponent } from './mijnteam/mijnteam.component';

const routes: Routes = [{ path: 'userprofile', component: UserpageComponent }, { path: 'dashboard', component: DashboardpageComponent }, { path: 'mijnteam', component: MijnteamComponent }, { path: '', redirectTo: '/dashboard', pathMatch: 'full' }, { path: '**', component: PagenotfoundComponent }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
