import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  constructor() { }

  userName: String;
  aantalNotificaties: number;

  notifications = [
    { id: 1, bericht: 'Nieuw verslag', link: "https://fbi.jessewilbrink.nl" },
    { id: 2, bericht: 'Nieuw bericht', link: "/" },
    { id: 5, bericht: 'Nieuwe update', link: "/" },
    { id: 3, bericht: 'Nieuwe user', link: "/" }
  ];

  ngOnInit() {
    this.userName = "Jesse Wilbrink";
    this.aantalNotificaties = this.notifications.length;
  }

}
