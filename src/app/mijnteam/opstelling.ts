import { ISpeler } from './speler';
import { IUser } from '../users/user';

export interface IOpstelling {
    id: number;
    spelers: ISpeler[];
    // user: IUser;
    budget: Number;
}