import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpstellingcreatorComponent } from './opstellingcreator.component';

describe('OpstellingcreatorComponent', () => {
  let component: OpstellingcreatorComponent;
  let fixture: ComponentFixture<OpstellingcreatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpstellingcreatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpstellingcreatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
