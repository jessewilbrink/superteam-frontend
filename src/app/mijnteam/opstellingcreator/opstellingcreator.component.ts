import { Component, OnInit } from '@angular/core';
import { spelerService } from '../service/speler.service';
import { ISpeler } from '../speler';

@Component({
  selector: 'app-opstellingcreator',
  templateUrl: './opstellingcreator.component.html',
  styleUrls: ['./opstellingcreator.component.css']
})
export class OpstellingcreatorComponent implements OnInit {
  spelers: ISpeler[];
  spelersnamen: String[];
  errorMessage: "Gegevens zijn niet opgehaald";
  constructor(private spelerService: spelerService) {

  }

  ngOnInit() {
    this.spelerService.getspelers().subscribe({
      next: spelers => {
        this.spelers = spelers
      },
      error: error => this.errorMessage = <any>error
    })

    this.naamFormatter();
  }

  naamFormatter() {
    this.spelers.forEach(speler => {
      if (speler.tussenvoegsel != "" || speler.tussenvoegsel != null) {
        this.spelersnamen.push(speler.voornaam + " " + speler.achternaam);
      } else {
        this.spelersnamen.push(speler.voornaam + ' ' + speler.tussenvoegsel + ' ' + speler.achternaam);
      }
    });
  }
}
