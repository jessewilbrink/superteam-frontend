import { TestBed } from '@angular/core/testing';
import { spelerService } from './speler.service';


describe('SpelerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: spelerService = TestBed.get(spelerService);
    expect(service).toBeTruthy();
  });
});
