import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, from, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import 'jquery';
import 'bootstrap-notify';
import { ISpeler } from '../speler';




@Injectable({
  providedIn: 'root'
})
export class opstellingService {
  private getUrl = "http://localhost:8080/speler/get/1";
  private getAllUrl = "http://localhost:8080/speler/getalle";
  private getAllMetWaardeClubLinieUrl = "";


  constructor(private http: HttpClient) { }

  getspeler(spelerID: Number): Observable<ISpeler> {
    // this.getUrl = this.getUrl + spelerID
    return this.http.get<ISpeler>(this.getUrl).pipe(
      tap(data => console.log('All: ' + JSON.stringify(data)))
      , catchError(this.handleError)
    );
  }

  getspelers(): Observable<ISpeler[]> {
    return this.http.get<ISpeler[]>(this.getAllUrl).pipe(
      tap(data => console.log('All: ' + JSON.stringify(data)))
      , catchError(this.handleError)
    );
  }

  getspelersMetWaardeClubLinie(linienummer: Number, club: String, waarde: Number): Observable<ISpeler[]> {
    this.getAllMetWaardeClubLinieUrl = "http://localhost:8080/speler/getalleMetWaardeEnClub/" + waarde.toString() + "/" + club + "/" + linienummer.toString();
    return this.http.get<ISpeler[]>(this.getAllMetWaardeClubLinieUrl).pipe(
      tap(data => console.log('All: ' + JSON.stringify(data)))
      , catchError(this.handleError)
    );
  }

  private handleError(err: HttpErrorResponse) {
    let errorMessage = " ";
    if (err.error instanceof ErrorEvent) {
      errorMessage = 'Er is een fout opgetreden: ${ err.error.message }';
    } else {
      errorMessage = 'De server retourneerd code: ${err.status}, foutbericht is: ${err.message}';
    }
    console.error(errorMessage);
    return throwError(errorMessage);
  }
}
