import { TestBed } from '@angular/core/testing';
import { opstellingService } from './opstelling.service';


describe('OpstellingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: opstellingService = TestBed.get(opstellingService);
    expect(service).toBeTruthy();
  });
});
