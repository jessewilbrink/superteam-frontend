import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MijnteamComponent } from './mijnteam.component';

describe('MijnteamComponent', () => {
  let component: MijnteamComponent;
  let fixture: ComponentFixture<MijnteamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MijnteamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MijnteamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
