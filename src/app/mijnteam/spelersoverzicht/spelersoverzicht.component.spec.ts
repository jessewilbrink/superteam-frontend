import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpelersoverzichtComponent } from './spelersoverzicht.component';

describe('SpelersoverzichtComponent', () => {
  let component: SpelersoverzichtComponent;
  let fixture: ComponentFixture<SpelersoverzichtComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpelersoverzichtComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpelersoverzichtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
