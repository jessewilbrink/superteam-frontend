export interface ISpeler {
    id: number;
    voornaam: String;
    tussenvoegsel: String;
    achternaam: String;
    club: String;
    positie: String;
    linienummer: number;
    waarde: number;
}