import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MijnopstellingComponent } from './mijnopstelling.component';

describe('MijnopstellingComponent', () => {
  let component: MijnopstellingComponent;
  let fixture: ComponentFixture<MijnopstellingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MijnopstellingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MijnopstellingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
