import { Component, OnInit } from '@angular/core';
import { ISpeler } from '../speler';
import { spelerService } from '../service/speler.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { IOpstelling } from '../opstelling';

@Component({
  selector: 'app-mijnopstelling',
  templateUrl: './mijnopstelling.component.html',
  styleUrls: ['./mijnopstelling.component.css']
})
export class MijnopstellingComponent implements OnInit {
  vindSpelerForm: FormGroup;
  spelers: ISpeler[];
  spelerGeselecteerd: ISpeler;
  opstelling: IOpstelling;
  spelerisleeg: boolean;
  linienummer: Number;
  errorMessage: "Gegevens zijn niet opgehaald";
  geselecteerdePositie: String;
  constructor(private spelerService: spelerService, private formBuilder: FormBuilder) {

  }

  ngOnInit() {
    this.vindSpelerForm = this.formBuilder.group({
      linienummer: '',
      waarde: '1',
      club: 'Ajax',
      speler: '',
    });
    this.onvindspelerFormChangesWaarde();
    this.onvindspelerFormChangesClub();
    this.onvindspelerFormChangesSpeler();
  }

  onvindspelerFormChangesWaarde(): void {
    this.vindSpelerForm.get('waarde').valueChanges.subscribe(val => {
      this.spelerService.getspelersMetWaardeClubLinie(this.vindSpelerForm.get('linienummer').value, this.vindSpelerForm.get('club').value, this.vindSpelerForm.get('waarde').value).subscribe({
        next: spelers => {
          this.spelers = spelers
        },
        error: error => this.errorMessage = <any>error
      })
    });
  }

  onvindspelerFormChangesClub(): void {
    this.vindSpelerForm.get('club').valueChanges.subscribe(val => {
      this.spelerService.getspelersMetWaardeClubLinie(this.vindSpelerForm.get('linienummer').value, this.vindSpelerForm.get('club').value, this.vindSpelerForm.get('waarde').value).subscribe({
        next: spelers => {
          this.spelers = spelers
        },
        error: error => this.errorMessage = <any>error
      })
    });
  }

  onvindspelerFormChangesSpeler(): void {
    this.vindSpelerForm.get('speler').valueChanges.subscribe(val => {
      this.spelerGeselecteerd = val;
    });
  }

  getSpelerClickEvent(positienummer: Number) {
    this.vindSpelerForm.get('waarde').setValue("1");
    this.vindSpelerForm.get('club').setValue("Ajax");
    this.vindSpelerForm.get('speler').setValue("");
    switch (positienummer) {
      case 1: {
        this.vindSpelerForm.get('linienummer').setValue(0);
        this.geselecteerdePositie = "keeper"
        break;
      }
      default: {
        if (positienummer > 1 && positienummer < 6) {
          this.vindSpelerForm.get('linienummer').setValue(1);
          this.geselecteerdePositie = "verdediger";
        } else if (positienummer > 5 && positienummer < 9) {
          this.vindSpelerForm.get('linienummer').setValue(2);
          this.geselecteerdePositie = "middenvelder";
        } else {
          this.vindSpelerForm.get('linienummer').setValue(3);
          this.geselecteerdePositie = "aanvaller";
        }

        break;
      }
    }

  }

  spelerisleegCheck(): boolean {
    if (this.spelers) {
      return this.spelers.length < 1;
    } else {
      return true;
    }
  }

  submitSpeler(): void {
    if (this.spelerGeselecteerd != null) {
      alert(this.spelerGeselecteerd.voornaam)
    } else {
      alert("Je hebt nog geen speler geselecteerd");
    }

  }

}
