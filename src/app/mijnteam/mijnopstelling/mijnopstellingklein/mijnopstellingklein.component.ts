import { Component, OnInit } from '@angular/core';
import { spelerService } from '../../service/speler.service';
import { ISpeler } from '../../speler';

@Component({
  selector: 'app-mijnopstellingklein',
  templateUrl: './mijnopstellingklein.component.html',
  styleUrls: ['./mijnopstellingklein.component.css']
})
export class MijnopstellingkleinComponent implements OnInit {
  spelers: ISpeler[];
  errorMessage: "Gegevens zijn niet opgehaald";

  constructor(private spelerService: spelerService) { }

  ngOnInit() {
    this.spelerService.getspelers().subscribe({
      next: spelers => {
        this.spelers = spelers
      },
      error: error => this.errorMessage = <any>error
    })
  }

}
