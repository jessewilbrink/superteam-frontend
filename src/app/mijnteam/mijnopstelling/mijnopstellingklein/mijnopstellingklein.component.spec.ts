import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MijnopstellingkleinComponent } from './mijnopstellingklein.component';

describe('MijnopstellingkleinComponent', () => {
  let component: MijnopstellingkleinComponent;
  let fixture: ComponentFixture<MijnopstellingkleinComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MijnopstellingkleinComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MijnopstellingkleinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
